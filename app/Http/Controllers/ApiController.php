<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provider;

class ApiController extends Controller
{
    public function providers()
    {
        return Provider::all();
    }
}

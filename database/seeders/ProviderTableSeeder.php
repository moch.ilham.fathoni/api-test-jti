<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $providers = [
            [
                'kode' => "081218",
                'nama' => "Telkomsel",
            ],
            [
                'kode' => "08140",
                'nama' => "Indosat",
            ],
            [
                'kode' => "0817",
                'nama' => "XL",
            ],
            [
                'kode' => "0885",
                'nama' => "Smartfren",
            ],
        ];

        \DB::table('provider')->insert($providers);
    }
}

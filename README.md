## Cara menjalankan backend (API) pada os windows

- Install git (jika belum ada), xampp (jika belum ada), composer (jika belum ada) dan nodejs (jika belum ada).
- Buka Git Bash terminal kemudian buka folder untuk menaruh project ini, kemudian jalankan perintah dibawah ini.
- git clone https://gitlab.com/moch.ilham.fathoni/api-test-jti.git
- cd api-test-jti
- composer install
- npm install
- cp .env.example .env
- php artisan key:generate
- kemudian buka phpmyadmin dan buat database dengan nama bebas, tinggal nanti disesuikan konfigurasinya di file .env
- check konfigurasi koneksi ke database pada file .env jika sudah benar maka jalankan perintah dibawah ini.
- php artisan migrate
- php artisan db:seed
- php artisan serve
